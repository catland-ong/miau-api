const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Setor'],
	summary: 'Busca um setor por id',
	params: {
		type: 'object',
		properties: {
			sectorId: {
				type: 'number',
				description: 'Sector id'
			}
		},
		required: ['sectorId']
	},
	response: {
		200: {
			type: 'object',
			properties: {
				id: {type: 'number'},
				name: {type: 'string'},
				description: {type: 'string'},
				slug: {type: 'string'}
			}
		},
		...errorSchema
	}
}
