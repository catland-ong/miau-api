const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Setor'],
	summary: 'Cria um novo setor',
	body: {
		type: 'object',
		properties: {
			name: { type: 'string', minLength: 2, maxLength: 50 },
			description: { type: 'string', maxLength: 255 },
		},
		required: ['name'],
	},
	response: {
		200: {
			type: 'object',
			properties: {
				id: {type: 'number'},
				name: {type: 'string'},
				description: {type: 'string'},
				slug: {type: 'string'}
			}
		},
		...errorSchema
	}
}
