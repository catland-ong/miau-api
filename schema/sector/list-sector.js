const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Setor'],
	summary: 'Lista todos os setores',
	response: {
		200: {
			type: 'array',
			items: {
				type: 'object',
				properties: {
					id: {type: 'number'},
					name: {type: 'string'},
					description: {type: 'string'},
					slug: {type: 'string'}
				}
			}
		},
		...errorSchema
	}
}
