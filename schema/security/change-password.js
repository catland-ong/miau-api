const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Security'],
	summary: 'Trocar a senha do usuário',
	body: {
		type: 'object',
		properties: {
			password: { type: 'string', minLength: 6, maxLength: 255 },
			confirmPassword: { type: 'string', minLength: 6, maxLength: 255 },
			hash: {type: 'string', minLength: 6}
		},
		required: ['password', 'confirmPassword', 'hash'],
	},
	response: {
		200: {
			type: 'object',
			properties: {

			}
		},
		...errorSchema
	}
}
