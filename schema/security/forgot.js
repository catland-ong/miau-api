const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Security'],
	summary: 'Recuperar a senha do usuário',
	body: {
		type: 'object',
		properties: {
			email: { type: 'string', format: 'email' }
		},
		required: ['email'],
	},
	response: {
		200: {
			type: 'object',
			properties: {

			}
		},
		...errorSchema
	}
}
