const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['Security'],
	summary: 'Autenticação do usuário',
	body: {
		type: 'object',
		properties: {
			email: { type: 'string', format: 'email' },
			password: { type: 'string' },
		},
		required: ['email', 'password'],
	},
	response: {
		200: {
			type: 'object',
			properties: {
				token: {type: 'string'}
			}
		},
		...errorSchema
	}
}
