const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['User'],
	summary: 'Lista um usuário por id',
	response: {
		200: {
			type: 'object',
			properties: {
				id: {type: 'number'},
				firstName: {type: 'string'},
				lastName: {type: 'string'},
				email: {type: 'string'},
				image: {type: 'string'},
				cpf: {type: 'string'},
				rg: {type: 'string'},
				birthday: {type: 'string'},
				phone: {type: 'string'},
				cellPhone: {type: 'string'},
				user_address: {
					type: 'object',
					properties: {
						address: {type: 'string'},
						number: {type: 'string'},
						complement: {type: 'string'},
						district: {type: 'string'},
						city: {type: 'string'},
						state: {type: 'string'},
						country: {type: 'string'},
						postalCode: {type: 'string'}
					}
				},
				user_company: {
					type: 'object',
					properties: {
						facebook: {type: 'string'},
						linkedin: {type: 'string'},
						car: {type: 'boolean'},
						cat: {type: 'number'},
						howDidYouMeet: {type: 'string'}
					}
				},
				user_sectors: {
					type: 'array',
					items: {
						type: 'object',
						properties: {
							sectorId: {type: 'number'}
						}
					}
				}
			}
		},
		...errorSchema
	}
}
