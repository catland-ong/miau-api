const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['User'],
	summary: 'Faz o upload da imagem do usuário',
	body: {
		type: 'object',
		properties: {
			id: {type: 'number'},
			file: {type: 'object'},
		},
		required: ['id', 'file'],
	},
	response: {
		200: {
			type: 'object',
			properties: {

			}
		},
		...errorSchema
	}
}
