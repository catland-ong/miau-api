const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['User'],
	summary: 'Edita um usuário',
	body: {
		type: 'object',
		properties: {
			id: {type: 'number'},
			firstName: { type: 'string', minLength: 3, maxLength: 255 },
			lastName: { type: 'string', minLength: 3, maxLength: 255 },
			email: { type: 'string', format: 'email', minLength: 3, maxLength: 255 },
			cpf: {type: 'string'},
			rg: {type: 'string'},
			birthday: {type: 'string'},
			phone: {type: 'string'},
			cellPhone: {type: 'string'},
			user_address: {
				type: 'object',
				properties: {
					address: {type: 'string'},
					number: {type: 'string'},
					complement: {type: 'string'},
					district: {type: 'string'},
					city: {type: 'string'},
					state: {type: 'string'},
					country: {type: 'string'},
					postalCode: {type: 'string'},
				},
				required: ['address', 'number', 'district', 'city', 'state', 'country', 'postalCode'],
			},
			user_company: {
				type: 'object',
				properties: {
					facebook: {type: 'string'},
					linkedin: {type: 'string'},
					car: {type: 'boolean'},
					cat: {type: 'number'},
					howDidYouMeet: {type: 'string'},
				},
				required: ['cat', 'howDidYouMeet']
			},
			sector: {
				type: 'array',
				items: {
					type: 'number'
				}
			}
		},
		required: ['id', 'lastName', 'email', 'cpf', 'rg', 'birthday', 'cellPhone', 'sector']
	},
	response: {
		200: {
			type: 'object',
			properties: {
				id: {type: 'number'},
				firstName: {type: 'string'},
				lastName: {type: 'string'},
				email: {type: 'string'}
			}
		},
		...errorSchema
	}
}
