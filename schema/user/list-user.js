const {errorSchema} = require('../../utils/constants')

module.exports = {
	tags: ['User'],
	summary: 'Lista todos os usuários',
	response: {
		200: {
			type: 'array',
			items: {
				type: 'object',
				properties: {
					id: {type: 'number'},
					firstName: {type: 'string'},
					lastName: {type: 'string'},
					email: {type: 'string'},
					image: {type: 'string'},
					cpf: {type: 'string'},
					rg: {type: 'string'},
					birthday: {type: 'string'},
					phone: {type: 'string'},
					cellPhone: {type: 'string'},
					createdAt: {type: 'string'}
				}
			}
		},
		...errorSchema
	}
}
