module.exports = (sequelize, DataTypes) => {

	const UserSector = sequelize.define('user_sector', {
		userId: {
			type: DataTypes.INTEGER,
			field: 'user_id',
			references: {
				model: sequelize.models.user,
				key: 'id',
			},
			allowNull: false
		},
		sectorId: {
			type: DataTypes.INTEGER,
			field: 'sector_id',
			references: {
				model: sequelize.models.sector,
				key: 'id',
			},
			allowNull: false
		},
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'user',
		freezeTableName: true
	})

	UserSector.associate = function(models) {
		// associations can be defined here
	}

	UserSector.findByUserId = function(userId) {
		return this.findAll({
			where: {
				userId: userId
			}
		})
	}

	return UserSector
}