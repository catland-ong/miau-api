module.exports = (sequelize, DataTypes) => {

	const EmailTemplate = sequelize.define('email_template', {
		name: { type: DataTypes.STRING, allowNull: false},
		slug: { type: DataTypes.STRING, allowNull: false, unique: true},
		templateId: { type: DataTypes.STRING, field: 'template_id', allowNull: false },
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'commons',
		freezeTableName: true
	})

	EmailTemplate.associate = function(models) {
		// associations can be defined here
	}

	EmailTemplate.findBySlug = async function (slug) {
		return EmailTemplate.findOne( {where: {slug}})
	}

	return EmailTemplate
}