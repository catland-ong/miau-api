module.exports = (sequelize, DataTypes) => {

	const Sector = sequelize.define('sector', {
		name: { type: DataTypes.STRING, allowNull: false },
		description: { type: DataTypes.STRING },
		slug: { type: DataTypes.STRING, allowNull: false, unique: true },
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'company',
		freezeTableName: true
	})

	Sector.findBySlug = async function(slug) {
		return this.findOne({
			where: {
				slug
			}
		})
	}

	return Sector
}