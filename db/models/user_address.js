module.exports = (sequelize, DataTypes) => {

	const Address = sequelize.define('user_address', {
		userId: {
			type: DataTypes.INTEGER,
			field: 'user_id',
			references: {
				model: sequelize.models.user,
				key: 'id',
			},
			allowNull: false
		},
		address: { type: DataTypes.STRING, allowNull: false },
		number: { type: DataTypes.STRING, allowNull: false },
		complement: { type: DataTypes.STRING },
		district: { type: DataTypes.STRING, allowNull: false },
		city: { type: DataTypes.STRING, allowNull: false },
		state: { type: DataTypes.STRING, allowNull: false },
		country: { type: DataTypes.STRING, allowNull: false },
		postalCode: { type: DataTypes.STRING, field: 'postal_code', allowNull: false },
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'user',
		freezeTableName: true
	})

	Address.associate = function(models) {
		// associations can be defined here
	}

	Address.findByUserId = function(userId) {
		return this.findOne({
			where: {
				userId: userId
			}
		})
	}

	return Address
}