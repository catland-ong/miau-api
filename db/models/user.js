const Sequelize = require('sequelize')
const Op = Sequelize.Op

module.exports = (sequelize, DataTypes) => {

	const User = sequelize.define('user', {
		firstName: { type: DataTypes.STRING, field: 'first_name', allowNull: false },
		lastName: { type: DataTypes.STRING, field: 'last_name', allowNull: false },
		email:  {type: DataTypes.STRING, allowNull: false, unique: true},
		image:  {type: DataTypes.STRING},
		cpf: {type: DataTypes.STRING, allowNull: false, unique: true},
		rg: {type: DataTypes.STRING, allowNull: false, unique: true},
		birthday: {type: DataTypes.DATEONLY, allowNull: false},
		phone: {type: DataTypes.STRING},
		cellPhone: {type: DataTypes.STRING, field: 'cell_phone', allowNull: false},
		password: { type: DataTypes.STRING, allowNull: false },
		forgotHash: { type: DataTypes.STRING, field: 'forgot_hash'},
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'user',
		freezeTableName: true
	})

	User.profileImageFolder = 'user/profile'

	User.associate = function(models) {
		this.hasOne(models.user_address)
		this.hasOne(models.user_company)
		this.hasMany(models.user_sector)
	}

	User.findById = function(userId) {
		return this.findOne({
			where: {
				id: userId
			},
			include: [{
				model: sequelize.models.user_address
			}, {
				model: sequelize.models.user_company
			}, {
				model: sequelize.models.user_sector
			}]
		})
	}

	User.findByEmail = function(email) {
		return this.findOne({
			where: {email}
		})
	}

	User.withoutEmail = async function(email) {
		return this.findAll({
			where: {
				email: {
					[Op.ne]: email
				}
			}
		})
	}

	User.findByEmailOrCpfOrRg = async function(email, cpf, rg) {
		return User.findOne({
			where: {
				[Op.or]: [{email}, {cpf}, {rg}]
			}
		})
	}

	User.findByForgotHash = async function(forgotHash) {
		return User.findOne({ where: {forgotHash} })
	}

	return User
}