module.exports = (sequelize, DataTypes) => {

	const Company = sequelize.define('user_company', {
		userId: {
			type: DataTypes.INTEGER,
			field: 'user_id',
			references: {
				model: sequelize.models.user,
				key: 'id',
			},
			allowNull: false
		},
		facebook: { type: DataTypes.STRING },
		linkedin: { type: DataTypes.STRING },
		car: { type: DataTypes.BOOLEAN },
		cat: { type: DataTypes.INTEGER, allowNull: false },
		howDidYouMeet: { type: DataTypes.TEXT, field: 'how_did_you_meet', allowNull: false },
		createdAt: { allowNull: false, type: DataTypes.DATE, field: 'created_at' },
		updatedAt: { allowNull: false, type: DataTypes.DATE, field: 'updated_at' },
	}, {
		schema: 'user',
		freezeTableName: true
	})

	Company.associate = function(models) {
		// associations can be defined here
	}

	Company.findByUserId = function(userId) {
		return this.findOne({
			where: {
				userId: userId
			}
		})
	}

	return Company
}