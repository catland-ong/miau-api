'use strict'

module.exports = {

	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert({
			tableName: 'email_template',
			schema: 'commons'
		}, [{
			name: 'Forgot password',
			slug: 'forgot-password',
			template_id: 'd-7c091783a81e4d0194a240f46c45d2fb',
			created_at: new Date(),
			updated_at: new Date()
		}], {})
	},

	down: (queryInterface, Sequelize) => {

	}

}
