'use strict'

module.exports = {

	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert({
			tableName: 'email_template',
			schema: 'commons'
		}, [{
			name: 'Welcome',
			slug: 'welcome',
			template_id: 'd-99fcbfed2fd64cda8e3c70c584edb988',
			created_at: new Date(),
			updated_at: new Date()
		}], {})
	},

	down: (queryInterface, Sequelize) => {

	}

}
