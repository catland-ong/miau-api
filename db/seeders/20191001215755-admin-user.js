'use strict'

module.exports = {

	up: (queryInterface, Sequelize) => {
		return queryInterface.bulkInsert({
			tableName: 'user',
			schema: 'user'
		}, [{
			first_name: 'Admin',
			last_name: 'User',
			email: 'admin@admin.com',
			image: null,
			cpf: '00000000000',
			rg: '000000000',
			birthday: new Date(),
			phone: '0000000000',
			cell_phone: '00000000000',
			password: '$2a$10$u91EaxrpylLarj5onzr2KerfylDOAv3d7A784rJxzoN7t39aeL09W',
			created_at: new Date(),
			updated_at: new Date()
		}], {})
	},

	down: (queryInterface, Sequelize) => {

	}

}
