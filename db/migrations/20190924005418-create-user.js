'use strict'

module.exports = {
	
	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createSchema('"user"', {transaction: t})

			await queryInterface.createTable('user', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				firstName: { type: Sequelize.STRING, field: 'first_name', allowNull: false },
				lastName: { type: Sequelize.STRING, field: 'last_name', allowNull: false },
				email:  {type: Sequelize.STRING, allowNull: false, unique: true},
				image:  {type: Sequelize.STRING},
				cpf: {type: Sequelize.STRING, allowNull: false, unique: true},
				rg: {type: Sequelize.STRING, allowNull: false, unique: true},
				birthday: {type: Sequelize.DATEONLY, allowNull: false},
				phone: {type: Sequelize.STRING},
				cellPhone: {type: Sequelize.STRING, field: 'cell_phone', allowNull: false},
				password: { type: Sequelize.STRING, allowNull: false },
				forgotHash: { type: Sequelize.STRING, field: 'forgot_hash'},
				createdAt: { type: Sequelize.DATE, field: 'created_at', allowNull: false },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at', allowNull: false  },
			}, {
				transaction: t,
				schema: 'user'
			})

			await t.commit()
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('user', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
