'use strict'

module.exports = {
	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createSchema('commons', {transaction: t})

			await queryInterface.createTable('email_template', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				name: { type: Sequelize.STRING, allowNull: false},
				slug: { type: Sequelize.STRING, allowNull: false, unique: true},
				templateId: { type: Sequelize.STRING, field: 'template_id', allowNull: false },
				createdAt: { type: Sequelize.DATE, field: 'created_at' },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at' },
			}, {
				transaction: t,
				schema: 'commons'
			})

			await t.commit()

		} catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('email_template', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
