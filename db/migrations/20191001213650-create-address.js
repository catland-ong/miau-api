'use strict'

module.exports = {

	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createTable('user_address', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				userId: {
					type: Sequelize.INTEGER,
					field: 'user_id',
					references: {
						model: {
							tableName: 'user',
							schema: 'user'
						},
						key: 'id'
					},
					allowNull: false,
					onDelete: 'CASCADE'
				},
				address: { type: Sequelize.STRING, allowNull: false },
				number: { type: Sequelize.STRING, allowNull: false },
				complement: { type: Sequelize.STRING },
				district: { type: Sequelize.STRING, allowNull: false },
				city: { type: Sequelize.STRING, allowNull: false },
				state: { type: Sequelize.STRING, allowNull: false },
				country: { type: Sequelize.STRING, allowNull: false },
				postalCode: { type: Sequelize.STRING, field: 'postal_code', allowNull: false },
				createdAt: { type: Sequelize.DATE, field: 'created_at', allowNull: false },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at', allowNull: false  },
			}, {
				transaction: t,
				schema: 'user'
			})

			await t.commit()
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('user_address', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
