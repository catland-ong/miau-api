'use strict'

module.exports = {

	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createTable('user_sector', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				userId: {
					type: Sequelize.INTEGER,
					field: 'user_id',
					references: {
						model: {
							tableName: 'user',
							schema: 'user'
						},
						key: 'id',
					},
					allowNull: false,
					onDelete: 'CASCADE'
				},
				sectorId: {
					type: Sequelize.INTEGER,
					field: 'sector_id',
					references: {
						model: {
							tableName: 'sector',
							schema: 'company'
						},
						key: 'id'
					},
					allowNull: false,
					onDelete: 'CASCADE'
				},
				createdAt: { type: Sequelize.DATE, field: 'created_at', allowNull: false },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at', allowNull: false  },
			}, {
				transaction: t,
				schema: 'user'
			})

			await t.commit()
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('user_sector', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
