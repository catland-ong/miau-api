'use strict'

module.exports = {

	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createTable('user_company', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				userId: {
					type: Sequelize.INTEGER,
					field: 'user_id',
					references: {
						model: {
							tableName: 'user',
							schema: 'user'
						},
						key: 'id'
					},
					allowNull: false,
					onDelete: 'CASCADE'
				},
				facebook: { type: Sequelize.STRING },
				linkedin: { type: Sequelize.STRING },
				car: { type: Sequelize.BOOLEAN },
				cat: { type: Sequelize.INTEGER, allowNull: false },
				howDidYouMeet: { type: Sequelize.TEXT, field: 'how_did_you_meet', allowNull: false },
				createdAt: { type: Sequelize.DATE, field: 'created_at', allowNull: false },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at', allowNull: false  },
			}, {
				transaction: t,
				schema: 'user'
			})

			await t.commit()
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('user_company', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
