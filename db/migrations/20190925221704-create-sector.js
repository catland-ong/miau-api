'use strict'

module.exports = {

	up: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {

			await queryInterface.createSchema('company', {transaction: t})

			await queryInterface.createTable('sector', {
				id: {
					allowNull: false,
					autoIncrement: true,
					primaryKey: true,
					type: Sequelize.INTEGER
				},
				name: { type: Sequelize.STRING, allowNull: false },
				description: { type: Sequelize.STRING },
				slug: { type: Sequelize.STRING, allowNull: false, unique: true },
				createdAt: { type: Sequelize.DATE, field: 'created_at', allowNull: false },
				updatedAt: { type: Sequelize.DATE, field: 'updated_at', allowNull: false  },
			}, {
				transaction: t,
				schema: 'company'
			})

			await t.commit()
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	},

	down: async (queryInterface, Sequelize) => {

		const t = await queryInterface.sequelize.transaction()

		try {
			await queryInterface.dropTable('sector', {transaction: t})
		}
		catch (err) {
			await t.rollback()
			throw err
		}

	}
}
