module.exports = {
	development: {
		username: 'postgres',
		password: null,
		database: 'miau',
		host: '127.0.0.1',
		dialect: 'postgres',
		pool: {
			max: 20,
			min: 1,
			acquire: 30000,
			idle: 10000
		},
		seederStorage: 'sequelize'
	},
	test: {
		username: 'postgres',
		password: null,
		database: 'miau',
		host: '127.0.0.1',
		dialect: 'postgres',
		pool: {
			max: 20,
			min: 1,
			acquire: 30000,
			idle: 10000
		},
		seederStorage: 'sequelize'
	},
	production: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOSTNAME,
		dialect: 'postgres',
		pool: {
			max: 20,
			min: 1,
			acquire: 30000,
			idle: 10000
		},
		seederStorage: 'sequelize'
	}
}
