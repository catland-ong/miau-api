module.exports = {
	exposeRoute: process.env.EXPOSE_SWAGGER === 'true',
	routePrefix: '/doc',
	swagger: {
		info: {
			title: 'Miau',
			version: '0.0.1',
		},
		externalDocs: {
			url: 'https://catland.org.br',
			description: 'Find more info here',
		},
		host: 'localhost',
		schemes: ['http'],
		consumes: ['application/json'],
		produces: ['application/json'],
	},
}
