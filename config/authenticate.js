const boom = require('@hapi/boom')

module.exports = async function (req, reply) {
	try {
		await req.jwtVerify()
	} catch (err) {
		throw boom.boomify(err, {statusCode: 401})
	}
}
