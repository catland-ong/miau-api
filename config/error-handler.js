module.exports = function (error, request, reply) {

	if (error.isBoom) {

		if (error.output && error.output.statusCode === 400) {
			reply.status(400).send({msg: error.output.payload.message})
		} else if (error.output && error.output.statusCode === 401) {
			reply.status(401).send({msg: 'error.unauthorized'})
		}
		else {
			console.log(error)
			reply.send({msg: 'error.internalServerError'})
		}

	}
	else if (error.validation
		|| error.statusCode === 400) {
		reply.status(400).send({msg: 'error.validation'})
	} else if (error.statusCode === 401) {
		reply.send({msg: 'error.unauthorized'})
	} else if (error.message === 'Not Found') {
		reply.send({msg: 'error.notFound'})
	} else {
		console.log(error)
		reply.send({msg: 'error.internalServerError'})
	}
}
