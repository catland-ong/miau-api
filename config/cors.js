
const cors = (process.env.CORS) ? process.env.CORS.split(',') : ['http://localhost:8080', 'http://localhost:5000']

module.exports = {
	origin: [cors],
	methods: '*',
	allowedHeaders: '*',
	exposedHeaders: '*'
}