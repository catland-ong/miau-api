const db = require('../../db/models')
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(process.env.EMAIL_API_KEY)

const from = {
	name: process.env.EMAIL_FROM_NAME || 'Miau',
	email: process.env.EMAIL_FROM_EMAIL || 'noreply@catland.org.br'
}

const email = async function (from, to, slug, data) {

	const emailTemplate = await db.email_template.findBySlug(slug)

	sgMail.send({
		from,
		to,
		template_id: emailTemplate.templateId,
		dynamic_template_data: data
	})

}

module.exports = {
	send: email,
	from: from
}
