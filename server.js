const fastify = require('fastify')({
	logger: {
		prettyPrint: true
	}
})

// file-upload
fastify.register(require('fastify-file-upload'))

// cors
fastify.register(require('fastify-cors'), require('./config/cors'))

// swagger
fastify.register(require('fastify-swagger'), require('./config/swagger'))

// compress plugin
fastify.register(require('fastify-compress'))

// helmet
fastify.register(require('fastify-helmet'), require('./config/helmet'))

// error handler
fastify.setErrorHandler(require('./config/error-handler'))

// jwt
fastify.register(require('fastify-jwt'), require('./config/jwt'))
fastify.decorate('authenticate', require('./config/authenticate'))

// api
fastify.register(require('fastify-loader'), {
	paths: ['./api/**/*.js'],
	inject: {
		JWT_SECRET: process.env.JWT_SECRET,
		UI_URL: process.env.UI_URL
	}
})

const server = async () => {

	try {

		await fastify.listen(process.env.PORT || 3000, '0.0.0.0')

		fastify.log.info(`server listening on ${fastify.server.address().port}`)

	} catch (err) {
		fastify.log.error(err)
		process.exit(1)
	}
}

fastify.ready((err) => {
	if (err) throw err
	fastify.swagger()
})

server()