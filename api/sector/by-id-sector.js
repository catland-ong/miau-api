/* global fastify */

const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.get('/v1/sector/:sectorId', {
	schema: require('../../schema/sector/by-id-sector')
}, async (req) => {

	try {
		return db.sector.findByPk(req.params.sectorId)
	} catch (e) {
		throw boom.boomify(e)
	}

})