/* global fastify */

const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.get('/v1/sector', {
	schema: require('../../schema/sector/list-sector')
}, async (req) => {

	try {

		return db.sector.findAll()

	} catch (e) {
		throw boom.boomify(e)
	}

})