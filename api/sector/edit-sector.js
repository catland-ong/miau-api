/* global fastify */

const slug = require('../../utils/slug')
const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.put('/v1/sector/:sectorId', {
	schema: require('../../schema/sector/edit-sector')
}, async (req) => {

	try {

		const model = req.body

		const sector = await db.sector.findByPk(req.params.sectorId)

		if (!sector) {
			return boom.badRequest('error.validation')
		}

		if (model.name) {

			model.slug = slug.slugify(model.name)

			const sectorBySlug = await db.sector.findBySlug(model.slug)

			if (model.slug === sector.slug || sectorBySlug) {
				return boom.badRequest('add.sector.form.error.alreadyExists')
			}

			sector.slug = model.slug
			sector.name = model.name

		}

		if (model.description) {
			sector.description = model.description
		}

		return sector.save()

	} catch (e) {
		throw boom.boomify(e)
	}

})