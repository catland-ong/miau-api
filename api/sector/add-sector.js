/* global fastify */

const slug = require('../../utils/slug')
const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.post('/v1/sector', {
	schema: require('../../schema/sector/add-sector')
}, async (req) => {

	try {

		const model = req.body

		model.slug = slug.slugify(model.name)

		const sector = await db.sector.findBySlug(model.slug)

		if (sector) {
			return boom.badRequest('add.sector.form.error.alreadyExists')
		}

		return db.sector.create(model)

	} catch (e) {
		throw boom.boomify(e)
	}

})