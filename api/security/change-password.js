/* global fastify, JWT_SECRET */

const boom = require('@hapi/boom')
const Cryptr = require('cryptr')
const moment = require('moment')
const bcrypt = require('bcryptjs')
const db = require('../../db/models')

fastify.post('/v1/security/change-password', {
	schema: require('../../schema/security/change-password')
}, async (req) => {

	try {

		const model = req.body

		if (model.password !== model.confirmPassword) {
			return boom.badRequest('error.validation')
		}

		const user = await db.user.findByForgotHash(model.hash)

		if (!user) {
			return boom.badRequest('security.changePassword.form.error.forgotPasswordExpired')
		}

		const cryptr = new Cryptr(JWT_SECRET)

		const hash = JSON.parse(cryptr.decrypt(user.forgotHash))

		const date = moment(hash.date).add(30, 'minutes')
		const now = moment(moment.now())

		if (!date.isAfter(now)) {
			return boom.badRequest('security.changePassword.form.error.forgotPasswordExpired')
		}

		user.password = bcrypt.hashSync(model.password, 10)
		user.forgotHash = null

		await user.save()

		return {}

	} catch (e) {
		throw boom.boomify(e)
	}

})
