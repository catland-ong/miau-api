/* global fastify, JWT_SECRET, UI_URL */

const boom = require('@hapi/boom')
const email = require('../../plugins/email')
const Cryptr = require('cryptr')
const moment = require('moment')
const db = require('../../db/models')

fastify.post('/v1/security/forgot-password', { schema: require('../../schema/security/forgot') }, async (req) => {

	try {

		const model = req.body

		const user = await db.user.findByEmail(model.email)

		if (!user) {
			return boom.badRequest('security.forgot.form.error.emailNotfound')
		}

		const cryptr = new Cryptr(JWT_SECRET)

		const hash = {
			date: moment.now(),
			id: user.id,
			email: user.email
		}

		user.forgotHash = cryptr.encrypt(JSON.stringify(hash))

		await user.save()

		email.send(email.from, user.email, 'forgot-password', {
			firstName: user.firstName,
			lastName: user.lastName,
			url: `${UI_URL}/security/forgot-password/${user.forgotHash}`
		}).then(null)

		return {}

	} catch (e) {
		throw boom.boomify(e)
	}

})
