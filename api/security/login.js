/* global fastify */

const boom = require('@hapi/boom')
const bcrypt = require('bcryptjs')
const db = require('../../db/models')

fastify.post('/v1/security/login', { schema: require('../../schema/security/login') }, async (req) => {

	try {

		const model = req.body

		const user =  await db.user.findByEmail(model.email)

		if (!user) {
			return boom.badRequest('security.login.form.error.emailOrPassword')
		}

		if (!bcrypt.compareSync(model.password, user.password)) {
			return boom.badRequest('security.login.form.error.emailOrPassword')
		}

		return {
			token: fastify.jwt.sign({
				id: user.id,
				firstName: user.firstName,
				lastName: user.lastName,
				email: user.email,
				image: user.image
			})
		}

	} catch (e) {
		throw boom.boomify(e)
	}

})