/* global fastify */

const boom = require('@hapi/boom')
const awsS3 = require('../../utils/aws-s3')
const db = require('../../db/models')

fastify.post('/v1/user/upload/image', {
	schema: require('../../schema/user/upload-image-user')
}, async (req) => {

	try {

		let model = req.body

		const user =  await db.user.findByPk(model.id)

		if (!user) {
			return boom.badRequest('error.validation')
		}

		user.image = awsS3.upload(db.user.profileImageFolder, model.file)

		user.save()

		return {}

	} catch (e) {
		throw boom.boomify(e)
	}

})