/* global fastify */

const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.get('/v1/user/:userId', {
	schema: require('../../schema/user/by-id-user')
}, async (req) => {

	try {

		const user = await db.user.findById(req.params.userId)

		if (!user) {
			return boom.badRequest('error.validation')
		}

		return user || {}

	} catch (e) {
		throw boom.boomify(e)
	}

})