/* global fastify */

const boom = require('@hapi/boom')
const moment = require('moment')
const _ = require('lodash')
const db = require('../../db/models')

fastify.put('/v1/user', {
	schema: require('../../schema/user/edit-user')
}, async (req) => {

	try {

		let model = req.body

		let user =  await db.user.findByPk(model.id)

		if (!user) {
			return boom.badRequest('error.validation')
		}

		model.birthday = moment(model.birthday, 'YYYY-MM-DD').toDate()

		const userAddress = await db.user_address.findByUserId(user.id)
		const userCompany = await db.user_company.findByUserId(user.id)
		const userSector = await db.user_sector.findByUserId(user.id)

		_.merge(user, model)
		_.merge(userAddress, model.user_address)
		_.merge(userCompany, model.user_company)

		await db.sequelize.transaction(t => {

			return Promise.all([
				user.save({transaction: t}),
				userAddress.save({transaction: t}),
				userCompany.save({transaction: t}),
				function () {
					for (const sector of userSector) {
						sector.destroy({transaction: t})
					}
					model.sector.map((sectorId) => db.user_sector.create({userId: user.id, sectorId}, {transaction: t}))
				}()
			])

		})

		return user

	} catch (e) {
		throw boom.boomify(e)
	}

})