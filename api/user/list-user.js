/* global fastify */

const boom = require('@hapi/boom')
const db = require('../../db/models')

fastify.get('/v1/user', {
	schema: require('../../schema/user/list-user')
}, async (req) => {

	try {
		return db.user.withoutEmail('admin@admin.com')
	} catch (e) {
		throw boom.boomify(e)
	}

})