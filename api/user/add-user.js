/* global fastify, JWT_SECRET, UI_URL */

const boom = require('@hapi/boom')
const uuid = require('uuid/v4')
const moment = require('moment')
const bcrypt = require('bcryptjs')
const Cryptr = require('cryptr')
const email = require('../../plugins/email')
const db = require('../../db/models')

fastify.post('/v1/user', {
	schema: require('../../schema/user/add-user')
}, async (req) => {

	try {

		let model = req.body

		let user =  await db.user.findByEmailOrCpfOrRg(model.email, model.cpf, model.rg)

		if (user) {
			return boom.badRequest('add.user.form.error.alreadyExists')
		}

		model.birthday = moment(model.birthday, 'YYYY-MM-DD').toDate()
		model.password = bcrypt.hashSync(uuid(), 10)

		await db.sequelize.transaction(t => {
			return db.user.create(model, {
				transaction: t
			}).then(r => {
				model.user_address.userId = r.id
				model.user_company.userId = r.id
				return Promise.all([
					db.user_address.create(model.user_address, {transaction: t}),
					db.user_company.create(model.user_company, {transaction: t}),
					model.sector.map((sectorId) => db.user_sector.create({userId: r.id, sectorId}, {transaction: t}))
				])
			})
		})

		user =  await db.user.findByEmailOrCpfOrRg(model.email, model.cpf, model.rg)

		const cryptr = new Cryptr(JWT_SECRET)

		const hash = {
			date: moment.now(),
			id: user.id,
			email: user.email
		}

		user.forgotHash = cryptr.encrypt(JSON.stringify(hash))

		await user.save()

		email.send(email.from, user.email, 'welcome', {
			id: user.id,
			firstName: user.firstName,
			lastName: user.lastName,
			email: user.email,
			url: `${UI_URL}/security/forgot-password/${user.forgotHash}`
		})

		return user

	} catch (e) {
		throw boom.boomify(e)
	}

})