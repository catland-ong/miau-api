const slugify = require('slugify')

module.exports = {
	slugify: (value) => slugify(value, {lower: true})
}