const errorSchema = {
	400: {
		type: 'object',
		properties: {
			msg: {type: 'string'}
		}
	},
	401: {
		type: 'object',
		properties: {
			msg: {type: 'string'}
		}
	},
	500: {
		type: 'object',
		properties: {
			msg: {type: 'string'}
		}
	}
}

module.exports = {errorSchema}