const path = require('path')
const uuid = require('uuid/v4')
const aws = require('aws-sdk')

const s3 = new aws.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
})

const upload = function (folder, file) {

	const fileName = uuid() + path.extname(file.name)
	const key = `${folder}/${fileName}`

	const params = {
		Bucket: process.env.AWS_BUCKET,
		Key: key,
		Body: file.data,
		ACL: 'public-read'
	}

	s3.upload(params, function (error, data) {
		if (error) {
			console.log(error)
		}
	})

	return key
}

const download =  function(key, callback) {

	const params = {
		Bucket: process.env.AWS_BUCKET,
		Key: key
	}

	return new Promise((resolve, reject) => {

		s3.getObject(params, (err, data) => {
			if (err) {
				reject(err)
			}
			else {
				data.meta = {
					contentType: `image/${path.extname(key).replace('.', '')}`
				}
				resolve(data)
			}
		})

	})

}

module.exports = { download, upload }